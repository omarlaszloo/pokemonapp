import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    Dimensions,
    TouchableWithoutFeedback
} from'react-native'
import { Text, Title } from 'react-native-paper'
import Carousel from 'react-native-snap-carousel'

const { width } = Dimensions.get('window')
const ITEM_WIDTH = Math.round(width * 0.7)

export default function CarouselVertical(props) {
    const { data, navigation } = props
    return(
        <Carousel
            layout={"default"}
            data={data}
            renderItem={(item) => <RenderItem data={item} navigation={navigation} />}
            sliderWidth={width}
            itemWidth={ITEM_WIDTH}
        />
    )
}

function RenderItem(props) {
    const { data, navigation } = props
    const { name, imagen, idPokemon } = data.item

    const onNavigation = () => {
        navigation.navigate('details', { id: idPokemon, name })
    }

    return(
        <TouchableWithoutFeedback onPress={onNavigation}>
            <View style={styles.card}>
                <Image style={styles.image} source={{uri: imagen}} />
                <Title style={styles.title}>{name}</Title>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    card: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 1,
        shadowRadius: 10,
    },
    image: {
        width: "100%",
        height: 450,
        borderRadius: 20
    },
    title: {
        marginHorizontal: 10,
        marginTop: 10
    }
})