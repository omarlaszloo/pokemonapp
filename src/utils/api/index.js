import axios from 'axios'

export  async function getPokemon () {

    const p1 = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=20')

    const p2 = await Promise.all([p1]).then((response => {
        let arrayPokemon = [];
        response[0].data.results.map((item, index) => {
            item.imagen = `https://pokeres.bastionbot.org/images/pokemon/${index + 1}.png`
            item.idPokemon = index + 1
            arrayPokemon.push(item)
            
        })
        return arrayPokemon
    }))

    return p2
}

export async function getDetailPokemonByID (idPokemon) {
    const p1 = await axios.get(`https://pokeapi.co/api/v2/pokemon/${idPokemon}`)
    let p2 = await Promise.all(p1.data.abilities.map(async (item, index) => {
        let abilityArray = []
        const ability = await axios.get(item.ability.url)
        abilityArray.push(ability.data.effect_entries[1])
        return abilityArray
    }))
    
    return p2[0]
}


export async function getDetailPokemonFormByID (idPokemon) {
    const p1 = await axios.get(`https://pokeapi.co/api/v2/pokemon-form/${idPokemon}`)
    return p1.data.sprites
}



