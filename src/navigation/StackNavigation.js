import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { IconButton } from 'react-native-paper'


import Home from '../screens/Home'
import Popular from '../screens/Popular'
import Details from '../screens/Details'
import Featured from '../screens/Featured'
import Search from '../screens/Search'

const Stack = createStackNavigator()

export default function StackNavigation(props) {
    const { navigation } = props
    const buttonLeft = (screen) => {
        switch(screen) {
            case 'Search':
                return (
                    <IconButton icon="arrow-left" onPress={() => navigation.goBack()} />
                )
            default:
                return (
                    <IconButton icon="menu" onPress={() => navigation.openDrawer()} />
                )
        }
    }

    const buttonRight = () => {
        return(
            <IconButton
                icon="magnify"
                onPress={() => navigation.navigate("Search")}
            />
        )
    }

    return (
        <Stack.Navigator>
            <Stack.Screen
                name="home"
                component={Home}
                options={{
                    title: 'ThePokemonApp',
                    headerLeft: () => buttonLeft("home"),
                    headerRight: () => buttonRight()
                }}
            />
            <Stack.Screen
                name="Popular"
                component={Popular}
                options={{ title: '',
                headerLeft: () =>  buttonLeft("Popular"),
                headerRight: () => buttonRight()}}
            />
            <Stack.Screen
                name="details"
                component={Details}
                options={{
                    title: 'detalle',
                    headerLeft: () =>  buttonLeft("details"),
                    headerRight: () => buttonRight()
                }}
            />
            <Stack.Screen
                name="Featured"
                component={Featured}
                options={{
                    title: 'destacados',
                    headerLeft: () =>  buttonLeft("Featured"),
                    headerRight: () => buttonRight()
                }}
            />
            <Stack.Screen
                name="Search"
                component={Search}
                options={{
                    title: '',
                    headerLeft: () =>  buttonLeft("Search"),
                    headerRight: () => buttonRight()
                }}
            />
        </Stack.Navigator>
    )
}
