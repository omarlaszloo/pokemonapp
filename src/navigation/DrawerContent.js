import React, { useState } from 'react'
import  { View, StyleSheet } from 'react-native'
import { DrawerContentScrollView } from '@react-navigation/drawer'
import usePreference from '../hooks/usePreferences'
import {
    Drawer,
    Switch,
    TouchableRipple,
    Text
} from 'react-native-paper'

export default function DrawerContent (props) {

    const { navigation } = props
    const [active, setActive] = useState('home')
    const { theme, toggleTheme } = usePreference()

    const onChangeScreen = (screen) => {
        setActive(screen)
        navigation.navigate(screen)
    }

    

    return (
        <DrawerContentScrollView>
            <Drawer.Section>
                <Drawer.Item
                    label="inicio"
                    active={active == "home"}
                    onPress={() => onChangeScreen('home')}
                />
                <Drawer.Item
                    label="Pokemon populares"
                    active={active == "details"}
                    onPress={() => onChangeScreen('details')}
                />
                <Drawer.Item
                    label="Pokemon destacados"
                    active={active == "Featured"}
                    onPress={() => onChangeScreen('Featured')}
                />

            </Drawer.Section>
            <Drawer.Section title="Opciones">
                <TouchableRipple>
                    <View style={styles.preferences}>
                        <Text>Tema Oscuro</Text>
                        <Switch value={theme === 'dark'}
                        onValueChange={toggleTheme}
                        />
                    </View>
                </TouchableRipple>
            </Drawer.Section>
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    preferences: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 12,
        paddingHorizontal: 16
    }
})