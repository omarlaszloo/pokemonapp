import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import { Title } from 'react-native-paper'
import { getPokemon } from '../utils/api'
import CarouselVertical from '../components/CarouselVertical'
 

export default function Home (props) {
    console.log('********', props.navigation.navigate)
    const { navigation } =  props
    const [pokemon, setPokemon] = useState([])

    useEffect(() => {
        const p1 = getPokemon().then(resp => {
            setPokemon(resp)
        })
    }, [])

    
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            {pokemon && (
                <View style={styles.pokemon}>
                    <Title style={styles.pokemonTitle}>Pokemon</Title>
                    <CarouselVertical data={pokemon} navigation={navigation} />
                </View>
            )}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    pokemon: {
        marginVertical: 10
    },
    pokemonTitle: {
        marginBottom: 15,
        marginHorizontal: 20,
        fontWeight: "bold",
        fontSize: 22
    }
})