import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import {Text} from 'react-native-paper'
import { getDetailPokemonByID, getDetailPokemonFormByID } from '../utils/api'

export default function Details (props) {
    const [pokemonDetails, setPokemonDetails] = useState()
    const [pokemonDetailsForm, setPokemonDetailsForm] = useState()


    console.log('DETALLES ====>', props.route.params)
    const { id, name } = props.route.params
    useEffect(() => {
        getDetailPokemonByID(id).then(response => {
            setPokemonDetails(response)
        })
        getDetailPokemonFormByID(id).then(responseForm => {
            setPokemonDetailsForm(responseForm)
        })
    }, [])

    const items = pokemonDetails !== undefined ? pokemonDetails : []
    const itemsForm = pokemonDetailsForm !== undefined ? pokemonDetailsForm : 'Sin imagen'

    console.log('pokemonDetailsForm', pokemonDetailsForm)

    return (
        <View style={styles.wraper}>
            <Text style={styles.title}>{name}</Text>
            <Text style={styles.subTitle}>Habilidades</Text>
            <View>
                {items.map((item) => {
                    console.log(item.language)
                    return (
                        <View style={styles.description}>
                            <Text>{item.effect}</Text>
                        </View>
                    )
                })}
            </View>
            <View>
                <Image style={styles.image} source={{uri: itemsForm.front_default}} />
                <Image style={styles.image} source={{uri: itemsForm.back_default}} />
                <Image style={styles.image} source={{uri: itemsForm.front_shiny}} />
                <Image style={styles.image} source={{uri: itemsForm.back_shiny}} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wraper: {
        marginHorizontal: 10,
        marginBottom: 15
    },
    title: {
        fontSize: 30,
        fontWeight: "bold",
        marginTop: 10
    },
    subTitle: {
        fontSize: 15,
        fontWeight: "bold",
        marginTop: 30
    },
    description: {
        marginBottom: 15,
        marginTop: 30
    },
    image: {
        width: "30%",
        height: 100,
        borderRadius: 20
    },
    image2: {
        width: "30%",
        height: 100,
        borderRadius: 20,
        marginTop: 30
    },
})